# NodeNgStarter

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Customizations

There isn't much different in this from a CLI application the only thing I added was the express server and a placeholder for a MongoDB instance. I typically create my servers and not use services like firebase for development as yet so I wanted to make sure I had a consistent experience and prevented myself from losing time doing the same steps over and over again.

This is already set up to leverage heroku for deployment just make sure you have the CLI and go to town.

### Important note

The local env.json file is excluded by default; you can copy the contents of the example one or just rename it and it should be good to go.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
