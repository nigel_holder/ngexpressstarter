const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const mongoose = require("mongoose");
const helmet = require('helmet');
const config = require('./config/config');
const app = express();
const http = require('http');


app.use(cors());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(helmet());
var server = http.createServer(app);
server.setTimeout(10 * 60 * 1000);
// Create link to Angular build directory
const distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Connect to the database before starting the application server.
mongoose.connect(config.MONGODB_URI, function (err, client) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  console.log("Database connection ready");

  //Routes
  app.use('', function () {
    return "test";
  });

  //Seed User


  // Initialize the app.

  const server = app.listen(config.PORT, function () {
    const port = server.address().port;
    console.log("App now running on port", port);
  });
});
