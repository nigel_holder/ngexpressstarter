var config = {};
var env = process.env.NODE_ENV || "development";
if (env === "development") {
  config = require("./env/development");
} else {
  config = require("./env/production");
}
module.exports = config;
