var envFile = __dirname + '/env.json';
var jsonfile = require('jsonfile');

var envVars = jsonfile.readFileSync(envFile);

module.exports = {
  MONGODB_URI: envVars["MONGODB_URI"],
  PORT: envVars["PORT"]
};
